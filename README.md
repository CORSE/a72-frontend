# A72 Frontend

A hand-crafted frontend model for the Cortex A72 (Raspberry Pi 4), to check how
relevant the (in-order) frontend is to accurate results.
