from palmed_model.model import PalmedModel
from enum import Enum, auto
import sys
from ruamel import yaml


class DispatchQueue(Enum):
    Branch = auto()
    Int = auto()
    Mult = auto()
    FP0 = auto()
    FP1 = auto()
    FP01 = auto()
    LdSt = auto()


class DispatchModel:
    _uops_for_insn: dict[str, int]
    _queues_for_insn: dict[str, list[DispatchQueue]]
    uops_of: dict[str, list[DispatchQueue]]

    def __init__(self, dispatch_path: str, uop_path: str):
        self._queues_for_insn = {}
        with open(dispatch_path, "r") as h:
            yaml_reader = yaml.YAML(typ='safe', pure=True)
            raw = yaml_reader.load(h)
        for insn, qus in raw.items():
            mapped_queues = [DispatchQueue[q] for q in qus]
            self._queues_for_insn[insn] = mapped_queues

        with open(uop_path, "r") as h:
            yaml_reader = yaml.YAML(typ='safe', pure=True)
            raw = yaml_reader.load(h)

        supported_insns = set(self._uops_for_insn) & set(self._queues_for_insn)

        assert len(supported_insns) == len(self._uops_for_insn)
        for insn in supported_insns:
            assert len(
                self._queues_for_insn[insn]) <= self._uops_for_insn[insn], insn

        self.uops_of = {}
        for insn in supported_insns:
            uop_count = self._uops_for_insn[insn]
            queues = self._queues_for_insn[insn]
            if uop_count != len(queues):
                if len(queues) > 1:  # Complicated, drop.
                    continue
                self.uops_of[insn] = uop_count * queues
            else:
                self.uops_of[insn] = queues
        for insn in self.uops_of:
            assert len(self.uops_of[insn]) == self._uops_for_insn[insn], insn
