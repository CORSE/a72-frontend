from palmed_model.model import PalmedModel
from enum import Enum, auto
import sys

model = PalmedModel.from_path(sys.argv[1])


class DispatchQueue(Enum):
    DBranch = auto()
    DInt = auto()
    DMult = auto()
    DFP0 = auto()
    DFP1 = auto()
    DFP01 = auto()
    DLdSt = auto()


res_to_dq = {
    "R3": DispatchQueue.DFP01,
    "R9": DispatchQueue.DFP1,
    "R14": DispatchQueue.DFP0,
    # "R15": DispatchQueue.DInt,
    "R22": DispatchQueue.DInt,
    "R23": DispatchQueue.DInt,
    "R13": DispatchQueue.DInt,
    "R20": DispatchQueue.DMult,
    "R12": DispatchQueue.DLdSt,
    "R17": DispatchQueue.DLdSt,
}

hardmap = {
    model.get_instr(k): v
    for k, v in {
        "ADCS_RD_W_RN_W_RM_W": DispatchQueue.DInt,
    }.items()
}


def resmap(insn):
    if insn in hardmap:
        return set([hardmap[insn]])
    res_usage = model.mapping[insn]
    qu = set()
    for res, val in res_usage.items():
        if val > 0.45 and res in res_to_dq:
            qu.add(res_to_dq[res])
    return qu


for cls in model.mapping:
    qu = resmap(cls)
    qu_str = " ".join([f"{x.name[1:]:<6}" for x in qu])
    print(f"[{len(model.class_to_instr[cls]):>4d}] {cls.name:<80} | {qu_str}")
