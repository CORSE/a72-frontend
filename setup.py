#!/usr/bin/env python3

from pathlib import Path
from setuptools import setup, find_packages
import pkg_resources
import sys


def parse_requirements():
    with Path('requirements.txt').open() as requirements_txt:
        install_requires = [
            str(requirement) for requirement in
            pkg_resources.parse_requirements(requirements_txt)
        ]
        return install_requires
    return []


setup(
    name="a72_frontend",
    version="0.1.0",
    description="Handcrafted frontend model of the Cortex A72",
    author="CORSE",
    license="LICENSE",
    url="https://gitlab.inria.fr/CORSE/a72_frontend",
    packages=find_packages(),
    include_package_data=True,
    package_data={
        "palmed_model": ["py.typed", "frontend_mapping.yml"],
    },
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={"console_scripts": []},
)
