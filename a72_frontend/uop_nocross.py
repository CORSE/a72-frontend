try:
    import typing_extensions as t
except ImportError:
    import typing as t  # type: ignore

import importlib.resources as pkg_resources
from fractions import Fraction

from ruamel import yaml

from .generic import BaseFrontend, InsnName, InstructionNotFound, Kernel

FRONT_MU_PER_CYCLE: int = 3


class Instruction:
    name: str
    front_mu: int

    _instructions: dict[InsnName, "Instruction"] = {}

    def __init__(self, name: str, front_mu: int):
        self.name = name
        self.front_mu = front_mu

    def register(self):
        """Add this Instruction to the instructions cache for ease of access"""
        self.__class__._instructions[self.name] = self

    @classmethod
    def load(cls):
        """Load the instructions from a yml file"""
        with pkg_resources.open_text(__package__,
                                     "frontend_mapping.yml") as handle:
            yaml_reader = yaml.YAML(typ='safe', pure=True)
            data: dict[str, int] = yaml_reader.load(handle)
        for insn, uops in data.items():
            cls(insn, uops).register()

    @classmethod
    def get(cls, insn: InsnName) -> "Instruction":
        try:
            return cls._instructions[insn]
        except KeyError as exn:
            raise InstructionNotFound(insn) from exn


Instruction.load()

InsnKernel: t.TypeAlias = list[Instruction]


class PureLinearModel(BaseFrontend):
    """Compute the throughput of a kernel, frontend-wise, only counting uops dispatched
    per cycle.
    Ignores separate dispatch queues."""

    insn_kernel: InsnKernel
    dispatch_pos: int
    cycle: int

    def _post_init(self):
        self.insn_kernel = list(map(Instruction.get, self.kernel))
        self.dispatch_pos = 0
        self.cycle = 0

    def _run_kernel(self):
        for insn in self.insn_kernel:
            n_pos = self.dispatch_pos + insn.front_mu
            self.dispatch_pos = n_pos % FRONT_MU_PER_CYCLE
            self.cycle += n_pos // FRONT_MU_PER_CYCLE

    def cycles(self) -> Fraction:
        # Reach steady-state: FRONT_MU_PER_CYCLES iterations suffice
        for _ in range(3):
            self._run_kernel()

        self.cycle = 0
        start_point = self.dispatch_pos
        kern_iters = 0

        for _ in range(3):
            self._run_kernel()
            kern_iters += 1
            if self.dispatch_pos == start_point:
                break

        return Fraction(self.cycle, kern_iters)


class UopNoCrossModel(BaseFrontend):
    """Compute the throughput of a kernel, frontend-wise, assuming that multi-uop
    instructions cannot be decoded across cycle boundaries. Ignores dispatch width."""

    insn_kernel: InsnKernel

    def _post_init(self):
        self.insn_kernel = list(map(Instruction.get, self.kernel))

    def _kern_end(self, start_mu: int) -> tuple[int, int]:
        """Given that the first available slot is start_mu, returns two numbers:
        * the next muop-slot available, after the kernel is decoded,
        * how many cycles were started by decoding this iteration of the kernel"""
        """ Hypothesis: multiple-muop instructions are not splitted across multiple
        cycles if they cannot be decoded in one go, but stall the frontend instead """

        pos = start_mu
        cycles_started = 0
        for insn in self.insn_kernel:
            if pos > 0 and pos + insn.front_mu > FRONT_MU_PER_CYCLE:
                pos = insn.front_mu % FRONT_MU_PER_CYCLE
                cycles_started += 1 + (insn.front_mu // FRONT_MU_PER_CYCLE)
            else:
                n_pos = pos + insn.front_mu
                pos = n_pos % FRONT_MU_PER_CYCLE
                cycles_started += n_pos // FRONT_MU_PER_CYCLE
        return (pos, cycles_started)

    def _kern_beg(self, start_mu: int) -> tuple[int, int]:
        """Same as _kern_end, but wraps around to 0 if the first instruction of the
        next iteration cannot be decoded in the remaining resources of this cycle"""

        end, cycles_started = self._kern_end(start_mu)
        if end + self.insn_kernel[0].front_mu > FRONT_MU_PER_CYCLE:
            return (0, cycles_started + 1)
        return end, cycles_started

    def cycles(self) -> Fraction:
        # Reach steady-state: FRONT_MU_PER_CYCLES iterations suffice
        start_point = 0
        for _ in range(3):
            start_point, _ = self._kern_beg(start_point)

        start_points = []
        cur_point = start_point
        cur_cycle = 0
        for _ in range(3):
            cur_point, add_cycle = self._kern_beg(cur_point)
            cur_cycle += add_cycle
            start_points.append(cur_point)
            if cur_point == start_point:
                break

        return Fraction(cur_cycle, len(start_points))
