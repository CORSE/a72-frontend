import importlib.resources as pkg_resources
import logging
import sys
import typing as t
from collections import defaultdict
from enum import Enum, auto
from fractions import Fraction

from ruamel import yaml

from .generic import BaseFrontend, InsnName, InstructionNotFound, Kernel

logger = logging.getLogger(__name__)


class DispatchQueue(Enum):
    Branch = auto()
    Int = auto()
    Mult = auto()
    FP0 = auto()
    FP1 = auto()
    FP01 = auto()
    LdSt = auto()


class DispatchModel:
    _uops_for_insn: dict[str, int]
    _queues_for_insn: dict[str, list[DispatchQueue]]
    uops_of: dict[str, list[DispatchQueue]] = {}

    def __init__(self):
        self._queues_for_insn = {}
        with pkg_resources.files(__package__).joinpath(
                "insn_to_queue.yml").open("r") as h:
            yaml_reader = yaml.YAML(typ='safe', pure=True)
            raw = yaml_reader.load(h)
        for insn, qus in raw.items():
            mapped_queues = [DispatchQueue[q] for q in qus]
            self._queues_for_insn[insn] = mapped_queues

        with pkg_resources.files(__package__).joinpath(
                "frontend_mapping.yml").open("r") as h:
            yaml_reader = yaml.YAML(typ='safe', pure=True)
            self._uops_for_insn = yaml_reader.load(h)

        supported_insns = set(self._uops_for_insn) & set(self._queues_for_insn)

        assert len(supported_insns) == len(self._uops_for_insn)
        for insn in supported_insns:
            assert len(
                self._queues_for_insn[insn]) <= self._uops_for_insn[insn], insn

        self.uops_of = {}
        for insn in supported_insns:
            uop_count = self._uops_for_insn[insn]
            queues = self._queues_for_insn[insn]
            if uop_count != len(queues):
                if len(queues) > 1:  # Complicated, drop.
                    continue
                self.uops_of[insn] = uop_count * queues
            else:
                self.uops_of[insn] = queues
        for insn in self.uops_of:
            assert len(self.uops_of[insn]) == self._uops_for_insn[insn], insn


dispatch_model = DispatchModel()

DECODE_MU_PER_CYCLE: int = 3
RENAME_MU_PER_CYCLE: int = 3
DISPATCH_MU_PER_CYCLE: int = 3

DISPATCH_PER_QUEUE: dict[DispatchQueue, int] = {
    DispatchQueue.Branch: 1,
    DispatchQueue.Int: 2,
    DispatchQueue.Mult: 2,
    DispatchQueue.FP0: 1,
    DispatchQueue.FP1: 1,
    DispatchQueue.FP01: 2,
    DispatchQueue.LdSt: 2,
}
DISPATCH_META: dict[DispatchQueue, DispatchQueue] = {
    DispatchQueue.FP0: DispatchQueue.FP01,
    DispatchQueue.FP1: DispatchQueue.FP01,
}


class BaseDispatchModelMixin:
    dispatch_queues: dict[DispatchQueue, int]

    def __init__(self):
        super().__init__()
        self._reset_disp_queues()

    def _reset_disp_queues(self):
        self.dispatch_queues = defaultdict(int)

    def _dispatch_if_possible(self, uop: DispatchQueue) -> bool:
        if self.dispatch_queues[uop] >= DISPATCH_PER_QUEUE[uop]:
            return False
        if uop in DISPATCH_META:
            m_uop = DISPATCH_META[uop]
            if self.dispatch_queues[m_uop] >= DISPATCH_PER_QUEUE[m_uop]:
                return False
            # Can dispatch
            self.dispatch_queues[m_uop] += 1
        self.dispatch_queues[uop] += 1
        return True


class SlackDispatchModel(BaseFrontend, BaseDispatchModelMixin):
    """Compute the throughput of a kernel, frontend-wise, in a slack-frontend
    situation: buffers are placed in between decoder and dispatcher. The model is more
    simulated and less algorithmic, fractions might look odd."""

    DECODED_QUEUE_SIZE: int = 64

    cycle: int
    kern_iter: int
    decoded_queue: list[DispatchQueue]
    renamed_limit: int
    insn_id: int

    def _post_init(self):
        self.cycle = 0
        self.kern_iter = 0
        self.insn_id = 0
        self.decoded_queue = []
        self.renamed_limit = 0

    def _new_cycle(self):
        self.cycle += 1
        self._reset_disp_queues()

    def _next_insn(self) -> InsnName:
        return self.kernel[self.insn_id]

    def _consume_insn(self):
        self.insn_id += 1
        if self.insn_id >= len(self.kernel):
            self.insn_id = self.insn_id % len(self.kernel)
            self.kern_iter += 1

    def _cycle(self):
        """Simulate one cycle"""
        # Decode
        for _ in range(DECODE_MU_PER_CYCLE):
            insn = self._next_insn()
            try:
                uops = dispatch_model.uops_of[insn]
            except KeyError:
                raise InstructionNotFound(insn)

            if len(self.decoded_queue) + len(uops) >= self.DECODED_QUEUE_SIZE:
                break
            self._consume_insn()
            self.decoded_queue += uops

        # Rename
        self.renamed_limit = min(len(self.decoded_queue),
                                 self.renamed_limit + RENAME_MU_PER_CYCLE)

        # Dispatch
        dispatched = 0
        for uop in self.decoded_queue:
            if dispatched >= DISPATCH_MU_PER_CYCLE or dispatched >= self.renamed_limit:
                break
            if not self._dispatch_if_possible(uop):
                break
            dispatched += 1
        self.decoded_queue = self.decoded_queue[dispatched:]
        self.renamed_limit -= dispatched

        self._new_cycle()

    def cycles(self) -> Fraction:
        # Reach steady-state:
        for _ in range(5 * (DECODE_MU_PER_CYCLE + self.DECODED_QUEUE_SIZE)):
            self._cycle()

        self.kern_iter = 0
        cycle_count = DECODE_MU_PER_CYCLE * DISPATCH_MU_PER_CYCLE * 20
        for _ in range(cycle_count):
            self._cycle()
        return Fraction(
            cycle_count, self.kern_iter
        )  # .limit_denominator( DECODE_MU_PER_CYCLE * DISPATCH_MU_PER_CYCLE)


class TenseDispatchModel(BaseFrontend, BaseDispatchModelMixin):
    """Compute the throughput of a kernel, frontend-wise, in a tense-frontend
    situation: the dispatcher is the frontend's only bottleneck (decoders always work
    fast enough); no buffers are considered. The model is quite algorithmic, and works
    kernel-per-kernel (instead of cycle-per-cycle), reaching fixed point."""

    dispatch_pos: int
    cycle: int

    def _post_init(self):
        self.dispatch_pos = 0
        self.cycle = 0

    def _new_cycle(self):
        self.cycle += 1
        self.dispatch_pos = 0
        self._reset_disp_queues()

    def _run_kernel(self):
        """Run one occurrence of the kernel"""

        for insn in self.kernel:
            try:
                uops = dispatch_model.uops_of[insn]
            except KeyError:
                raise InstructionNotFound(insn)

            for uop in uops:
                if not self._dispatch_if_possible(uop):
                    self._new_cycle()
                    assert self._dispatch_if_possible(uop)
                self.dispatch_pos += 1
                if self.dispatch_pos >= DISPATCH_MU_PER_CYCLE:
                    self._new_cycle()

    def cycles(self) -> Fraction:
        # Reach steady-state: DISPATCH_MU_PER_CYCLE iterations suffice
        """
        Note: at the end of a kernel, the state is entirely determined by
        :self.dispatch_pos:. Indeed,
        * if dispatch_pos = 0, then necessarily the dispatch queues are empty;
        * if dispatch_pos = k > 0, as there are no bubbles, the uops executed at this
          cycle are the last k uops of the kernel, which are always the same
        """
        for _ in range(DISPATCH_MU_PER_CYCLE):
            self._run_kernel()

        self.cycle = 0
        start_point = self.dispatch_pos
        kern_iters = 0
        for _ in range(3):
            self._run_kernel()
            kern_iters += 1
            if self.dispatch_pos == start_point:
                # fixpoint is reached
                break
        logger.debug(
            "Reached fixpoint in %d iterations (%d cycles), starting from pos %d",
            kern_iters,
            self.cycle,
            start_point,
        )
        return Fraction(self.cycle, kern_iters)
