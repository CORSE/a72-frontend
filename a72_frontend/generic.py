try:
    import typing_extensions as t
except ImportError:
    import typing as t  # type: ignore

from abc import ABC, abstractmethod
from enum import Enum, auto
from fractions import Fraction

InsnName: t.TypeAlias = str
Kernel: t.TypeAlias = list[InsnName]


class InstructionNotFound(Exception):
    pass


class BaseFrontend:
    kernel: Kernel

    def __init__(self, kernel: Kernel):
        super().__init__()
        self.kernel = kernel
        self._post_init()

    def _post_init(self):
        pass

    @abstractmethod
    def cycles(self) -> Fraction:
        """How many cycles does one iteration of the kernel take, in steady state,
        frontend-wise"""
