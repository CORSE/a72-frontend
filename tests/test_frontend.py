from a72_frontend import frontend
from fractions import Fraction


def initialize():
    frontend.Instruction(name="1", front_mu=1).register()
    frontend.Instruction(name="2", front_mu=2).register()
    frontend.Instruction(name="3", front_mu=3).register()
    frontend.Instruction(name="7", front_mu=7).register()


def test_kern_end():
    c1 = frontend.FrontendCarac(["1"])
    assert c1._kern_end(0) == (1, 0)
    assert c1._kern_end(1) == (2, 0)
    assert c1._kern_end(2) == (0, 1)

    c2 = frontend.FrontendCarac(["1", "2"])
    assert c2._kern_end(0) == (0, 1)
    assert c2._kern_end(1) == (2, 1)
    assert c2._kern_end(2) == (2, 1)

    c3 = frontend.FrontendCarac(["2", "2", "1"])
    assert c3._kern_end(0) == (0, 2)
    assert c3._kern_end(1) == (0, 2)
    assert c3._kern_end(2) == (0, 3)

    c4 = frontend.FrontendCarac(["7"])
    assert c4._kern_end(0) == (1, 2)
    assert c4._kern_end(1) == (1, 3)
    assert c4._kern_end(2) == (1, 3)


def test_kern_beg():
    c1 = frontend.FrontendCarac(["1"])
    assert c1._kern_beg(0) == (1, 0)
    assert c1._kern_beg(1) == (2, 0)
    assert c1._kern_beg(2) == (0, 1)

    c2 = frontend.FrontendCarac(["1", "2"])
    assert c2._kern_beg(0) == (0, 1)
    assert c2._kern_beg(1) == (2, 1)
    assert c2._kern_beg(2) == (2, 1)

    c3 = frontend.FrontendCarac(["2", "2", "1"])
    assert c3._kern_beg(0) == (0, 2)
    assert c3._kern_beg(1) == (0, 2)
    assert c3._kern_beg(2) == (0, 3)

    c4 = frontend.FrontendCarac(["7"])
    assert c4._kern_beg(0) == (0, 3)
    assert c4._kern_beg(1) == (0, 4)
    assert c4._kern_beg(2) == (0, 4)

    c5 = frontend.FrontendCarac(["2", "1", "1"])
    assert c5._kern_beg(0) == (1, 1)
    assert c5._kern_beg(1) == (0, 2)


def test_cycles():
    assert frontend.FrontendCarac(["1"]).cycles() == Fraction(1, 3)
    assert frontend.FrontendCarac(["2"]).cycles() == Fraction(1, 1)
    assert frontend.FrontendCarac(["1", "2"]).cycles() == Fraction(1, 1)
    assert frontend.FrontendCarac(["2", "2", "1"]).cycles() == Fraction(2, 1)
    assert frontend.FrontendCarac(["7"]).cycles() == Fraction(3, 1)
    assert frontend.FrontendCarac(["1", "2", "1"]).cycles() == Fraction(3, 2)


initialize()
